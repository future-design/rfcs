# Tezos RFCs

This repository contains RFCs for changes to the Tezos protocol. Don't waste reviewers' time with MRs they don't understand. Don't waste your friends'/colleagues' time telling them your opinions about what should be added to Tezos. Just write an RFC!

## How to Contribute

Open an issue using the [RFC template](template.md). Make a good argument for why your change should be made to the protocol. People will then comment on the issue (that's the 'C' in 'RFC') and it will be tagged with some sort of lifecycle (TBD). If enough people think the change should be added to a future protocol upgrade, the RFC will be merged into this repo under the issue number. 

You don't need to be able to implement a feature in order to write an RFC for it, but it's probably a good idea if you at least know someone you think you can convince to implement it.
