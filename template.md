---
title: RFC title
author: Name(s) of author(s), ideally with their username(s) or email address(es)
requires: Optional. Comma-separated RFC numbers
replaces: Optional. Comma-separated RFC numbers
---


## Summary

Provide a simplified and layman-accessible explanation of the RFC.

## Abstract

A short (200-500 word) but comprehensive description of the issue being
addressed and the proposed solution.

## Motivation

It should clearly explain why the existing implementation is inadequate to
address the problem that the RFC solves.

## Specification

The technical specification should describe the syntax and semantics of any new
feature.

## Rationale

The rationale fleshes out the specification by describing what motivated the
design and why particular design decisions were made. It should describe
alternate designs that were considered and related work. The rationale may also provide evidence of
consensus within the community, and should discuss important objections or
concerns raised during discussion.

## Backwards Compatibility

All RFCs that introduce backwards incompatibilities or supersede other RFCs
must include a section describing these incompatibilities, their severity, and
solutions.

## Test Cases

Test cases for an implementation are recommended as are proofs of correctness via formal methods if applicable.

## Implementations

Any code already written.

## Appendix

A list of references relevant to the proposal.

## Copyright

All RFCs must be in the public domain, or a under a permissive license
substantially identical to placement in the public domain. Example of a
copyright waiver:

Copyright and related rights waived via
[CC0](https://creativecommons.org/publicdomain/zero/1.0/).
